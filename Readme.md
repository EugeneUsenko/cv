# Summary of Qualifications

Total working experiance is more than 10 years in the following domains:

* aerospace
* medicine
* ditial video systems
* social
* gambling
* data presentation

# Skills 

| Item                      | Description                                        
| ------------------------- | -------------------------------------------------- 
| **Programming Languages** | Java(primary), JS(primary), Python and Sehll scripting, C/C++   
| **Frameworks**            | Java SE, [JDBC][19], [JPA][14], [EJB][20], [Android][21], Spring, Swing, GWT, ReactJS/Redux, NodeJS
| **Application Servers**   | Jetty, Tomcat, JBoss, NodeJS
| **VCS**                   | Git(primary), Mercurial, Subversion
| **CI**                    | TeamCity, Jenkins
| **Tools**                 | Maven, Gradle, Docker, npm
| **Operating Systems**     | Linux
| **Methodologies**         | SCRUM, TDD     

# Experiance

## [DBoard](http://www.dboard.lu/) 

Business management tool which functions are:

* Warehouse of the business object and their relations
* Process management and visualization
* Document management

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | BE company
| **Involvement Duration**  | 3.5 years(till now)
| **Project Role**          | Teamlead
| **Responsibilities**      | Technical design, code analyse, refactoring, task decomposition and delegation, technical documentation, code review, promotion to production
| **Project Team Size**     | 5-15 team members
| **Tools & Technologies**  | Java SE, GWT, Tomcat, React/Redux


## [Horizon Platform](http://luxofthorizon.com/)

Data visualization framework based on java and web technologies. 

| Item                      | Description                                      
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 2 years
| **Project Role**          | Senior Developer
| **Responsibilities**      | Contribution to the project, bugfix
| **Project Team Size**     | 5-15 team members(distributed team - Poland/Ukraine)
| **Tools & Technologies**  | Java SE, GWT, Spring IoC

## [Caesars Casino](https://www.caesarsgames.com/)

Project is a social casino which is a highload application(~100k DAO) with michroservice architecture. Areas and features I worked on:

* New feture implementation
* REST API design
* Integration with other subsystems(services)

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | IL company
| **Involvement Duration**  | 8 month
| **Project Role**          | Teachlead
| **Responsibilities**      | Technical design, code analyse, refactoring, task decomposition and delegation, technical documentation, code review
| **Project Team Size**     | 5-15 team members
| **Tools & Technologies**  | Java SE, [JDBC][19], Spring IoC, [JAX-RS][22], Couchbase, MySQL, Tomcat
  
## [WU Phone](https://play.google.com/store/apps/details?id=com.wunderground.android.weather)

_Weather forecast_ application with advanced features(map animation). Features I worked on:

* Performance optimization
* Data flow optimization 
* Refactoring/redesign rendering subsystem

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 9 month
| **Project Role**          | Teachlead
| **Responsibilities**      | Technical design, code analyse, refactoring
| **Project Team Size**     | 2 team members
| **Tools & Technologies**  | [Android][21], Google Maps API v1/v2, Amazon Maps

## Social (pilot project)

Engine for social network.

* Technial design 
* REST API design
* Feature implementation
* Integration with different social networks like Facebook, Twitter, etc

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 9 month
| **Project Role**          | Senior Developer
| **Responsibilities**      | Technical design
| **Project Team Size**     | 5-8 team members
| **Tools & Technologies**  | JavaSE, Spring Data/MVC/IoC/Security, MongoDB, Redis, Tomcat

## [Bemo][23] (pilot project)

[Android][21] client for the wearable bluetooth [action camera](https://www.amazon.com/Logitech-Bemo-Social-Video-Camera/dp/B00LP1KMX8/ref=pd_lpo_sbs_147_t_1?_encoding=UTF8&psc=1&refRID=SR5X7A4K0C02JFRCMXTH) with following features:

* Video live streaming
* Video generator and video editor
* Gallary management

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 9 month
| **Project Role**          | Teachlead
| **Responsibilities**      | Technical design
| **Project Team Size**     | 3 team members
| **Tools & Technologies**  | [Android][21], JavaSE, [JNI][10], [FFMPEG][11]

## [Gazzang self test system](http://www.cloudera.com/products/security.html)

Self testing system for cloud based storage.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 2 month
| **Project Role**          | Developer
| **Responsibilities**      | Technical design, feature implementation
| **Project Team Size**     | 3 team members
| **Tools & Technologies**  | Python

## [Alert](https://play.google.com/store/apps/details?id=com.logitech.android)

[Android][21] client for the video security system. Features I worked on:

* Live streaming with low latency
* Video player optimization(playback) 
* Notification center

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 14 month
| **Project Role**          | Developer/Technical leader
| **Responsibilities**      | Technical design, feature implementation, refactoring, coaching
| **Project Team Size**     | 2-5 team members
| **Tools & Technologies**  | [Android][21], [JNI][10], [FFMPEG][11]

## iText (pilot project)

Hardware‐software home security complex with a hub based on a custom android device. Main functions of the app are:

* Gather and analyse data from sensors(ZigBee)
* Notification center
* Management of the historical data

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 5 month
| **Project Role**          | Technical leader
| **Responsibilities**      | Technical design, feature implementation, coaching
| **Project Team Size**     | 2-5 team members
| **Tools & Technologies**  | [Android][21], [JNI][10]

## [WorldApp KeySurvey(Desktop version)](https://www.keysurvey.com/)

Desktop version of the _KeySurvey_ web application.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 2 month
| **Project Role**          | Technical leader
| **Responsibilities**      | Technical design, feature implementation, coaching
| **Project Team Size**     | 2-5 team members
| **Tools & Technologies**  | JavaSE, Swing, [JPA][14], HSQLDB, [JAX‐WS][13], [ANTLR][12]

## [Basecamp](http://#)

Application for financial department which main purpose is data visualization.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 3.5 month
| **Project Role**          | Developer, Scrum Master
| **Responsibilities**      | Technical design, feature implementation, coaching
| **Project Team Size**     | 2-6 team members
| **Tools & Technologies**  | JavaSE, [JDBC][19], Spring IoC/MVC, [jFreeChart][15], Tomcat

## [Usability Tools](http://#)

Web application which purpose is to create/run different type of survays and make analysis of the collected data.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 2.5 month
| **Project Role**          | Developer, Scrum Master
| **Responsibilities**      | Feature implementation, coaching
| **Project Team Size**     | 3-6 team members
| **Tools & Technologies**  | JavaSE, [JPA][14], JSP, Spring IoC/MVC/Security, MySQL, Tomcat

## [Gene](http://isd.dp.ua/)

Distributed application for hospitals/clinics/labs which main purpose is automation of the internal process.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 4 month
| **Project Role**          | Developer
| **Responsibilities**      | Feature implementation, integration with other subsystems
| **Project Team Size**     | 50+ team members(distributed team across Poland/Ukraine)
| **Tools & Technologies**  | JavaSE, [JPA][14], [EJB][20], [JAX‐WS][13], JBoss

## [Total QC](http://isd.dp.ua/)

Distributed application for for hospitals/clinics/labs to controll quality of the medical equipment, reagents, etc.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | US company
| **Involvement Duration**  | 16 month
| **Project Role**          | Developer
| **Responsibilities**      | Feature implementation, integration with other subsystems
| **Project Team Size**     | 5-30 team members(distributed team across USA/Poland/Ukraine)
| **Tools & Technologies**  | JavaSE, [JPA][14], [EJB][20], [JAX‐WS][13], JBoss

## [S3‐IDE](http://www.rts-soft.com/en/products/s3/)

Multiplatform IDE for all functions and all levels of automated control system.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 14 month
| **Project Role**          | Developer
| **Responsibilities**      | Feature implementation
| **Project Team Size**     | 3 team members
| **Tools & Technologies**  | JavaSE, Swing, [ANTLR][12], [Netbeans RCP][16], [QNX][17]

## [Octavo](http://www.rts-soft.com/en/products/octavo/)

Hardware‐software complex for automation of a test and metrological verification, automation of parameters registration process and data visualization in real‐time mode.

| Item                      | Description                                       
| ------------------------- | -------------------------------------------------- 
| **Customer**              | UA company
| **Involvement Duration**  | 6 month
| **Project Role**          | Junior developer
| **Responsibilities**      | Feature implementation
| **Project Team Size**     | 3 team members
| **Tools & Technologies**  | JavaSE, [SWT][18], [QNX][17]

# Education

* [PSACEA: Automation of Technological & Manufacturing Processes](http://pgasa.dp.ua/en/) - diploma with honors
* [OCJP 1.6(1Z0-851)](data/1Z0-851.pdf)
* [M101JS](http://university.mongodb.com/course_completion/1ad65bdb-de01-42b0-8fa8-5de09f83)

[10]: http://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/jniTOC.html 
[11]: https://ffmpeg.org/
[12]: http://www.antlr.org/
[13]: http://docs.oracle.com/javase/7/docs/technotes/guides/xml/jax-ws/
[14]: http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html
[15]: http://www.jfree.org/jfreechart/
[16]: https://netbeans.org/features/platform/
[17]: http://www.qnx.com/content/qnx/en.html
[18]: https://www.eclipse.org/swt/
[19]: http://www.oracle.com/technetwork/java/javase/jdbc/index.html
[20]: http://www.oracle.com/technetwork/java/index-jsp-140203.html
[21]: https://www.android.com/
[22]: https://jax-rs-spec.java.net/
[23]: https://mybemo.com/